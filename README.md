# 2020S2-FIT5042Demo

Some tutorial materials and exercises 

## Week 1 [Studio PDF](PDF/FIT5042Studio1.pdf)

Task 1.1 Factory design pattern and singleton desgin pattern 
```
W1ExeStudent
```

Task 1.2 JavaEE architecture 
```
W1ExeStudent-common
W1ExeStudent-ejb
W1ExeStudent-war
W1EEStudent
```

## Week 2 [Studio PDF](PDF/FIT5042Studio2.pdf)

Task 2.1 JSF
```
W2ExeStudent-war
```