/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.tutex.repository;


import java.util.ArrayList;
import java.util.List;

import fit5042.tutex.repository.entities.Property;

/**
 * TODO Exercise 1.3 Step 2 Complete this class.
 * 
 * This class implements the PropertyRepository class. You will need to add the keyword
 * "implements" PropertyRepository. 
 * 
 * @author Zhenkun Xie 30760208
 */
public class SimplePropertyRepositoryImpl implements PropertyRepository {

	private static List<Property> propertyList = new ArrayList<Property>();


	@Override
	public void addProperty(Property property) throws Exception {
		propertyList.add(property);
		
	}

	@Override
	public Property searchPropertyById(int id) throws Exception {
		for(Property p : propertyList) {
			if(p.getId() == id) {
				return p;
			}
		}
		
		return null;
	}

	@Override
	public List<Property> getAllProperties() throws Exception {		
		return propertyList;
	}
}
