package fit5042.tutex.repository.entities;

import java.math.BigDecimal;
import java.text.NumberFormat;

/**
 *
 * @author Zhenkun Xie 30760208
 * 
 */
//TODO Exercise 1.3 Step 1 Please refer tutorial exercise. 
public class Property {
    private int id;
    private String address;
    private int numOfBedrooms;
    private int size;
    private BigDecimal price;
     
	public Property() {}

	public Property(int id, String address, int numOfBedrooms, int size, BigDecimal price) {
		super();
		this.id = id;
		this.address = address;
		this.numOfBedrooms = numOfBedrooms;
		this.size = size;
		this.price = price;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getNumOfBedrooms() {
		return numOfBedrooms;
	}

	public void setNumOfBedrooms(int numOfBedrooms) {
		this.numOfBedrooms = numOfBedrooms;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public int getId() {
		return id;
	}

	@Override
	public String toString() {
		return id + " " + address + " " + numOfBedrooms + "BR(s) " + size + "sqm " + 
				NumberFormat.getCurrencyInstance().format(price);
	}
}
