package fit5042.tutex;

import fit5042.tutex.repository.PropertyRepository;
import fit5042.tutex.repository.PropertyRepositoryFactory;
import fit5042.tutex.repository.SimplePropertyRepositoryImpl;
import fit5042.tutex.repository.entities.Property;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * TODO Exercise 1.3 Step 3 Complete this class. Please refer to tutorial instructions.
 * This is the main driver class. This class contains the main method for Exercise 1A
 * 
 * This program can run without the completion of Exercise 1B.
 * 
 * @author Zhenkun Xie 30760208
 */
public class RealEstateAgency {
    private String name;
    private final PropertyRepository propertyRepository;
    
    public RealEstateAgency(String name) throws Exception {
        this.name = name;
        this.propertyRepository = PropertyRepositoryFactory.getInstance();
    }
    
    // this method is for initializing the property objects
    // complete this method
    public void createProperty() {
    	
    	try {
			propertyRepository.addProperty(new Property(0, "k", 1, 100, new BigDecimal(4000.00)));
			propertyRepository.addProperty(new Property(1, "kk", 2, 200, new BigDecimal(5000.00)));
			propertyRepository.addProperty(new Property(2, "kkk", 3, 300, new BigDecimal(6000.00)));
			propertyRepository.addProperty(new Property(3, "kkkk", 4, 400, new BigDecimal(7000.00)));
			propertyRepository.addProperty(new Property(4, "kkkkk", 5, 500, new BigDecimal(8000.00)));
			
			System.out.println("5 properties added successfully!");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    }
    
    // this method is for displaying all the properties
    // complete this method
    public void displayProperties() {
        List<Property> pList;
		try {
			pList = propertyRepository.getAllProperties();
			for(Property p : pList) {
	        	System.out.println(p.toString());
	        }
		} catch (Exception e) {
			e.printStackTrace();
		}
        
    }
    
    // this method is for searching the property by ID
    // complete this method
    public void searchPropertyById() {
    	System.out.print("Enter the ID of the preperty you want to search:");
    	Scanner sc = new Scanner(System.in);
    	
    	Property p = null;
 
    	try {
    		int searchId = sc.nextInt();
			p = propertyRepository.searchPropertyById(searchId);
			System.out.println(p.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
    	
    }
    
    public void run() throws Exception {
        createProperty();
        System.out.println("********************************************************************************");
        displayProperties();
        System.out.println("********************************************************************************");
        searchPropertyById();
    }
    
    public static void main(String[] args) {
        try {
        	RealEstateAgency rea = new RealEstateAgency("FIT5042 Real Estate Agency");
        	rea.run();
        } catch (Exception ex) {
            System.out.println("Application fail to run: " + ex.getMessage());
        }
    }
}
