package fit5042.tutex.controllers;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.annotation.FacesConfig;
import javax.inject.Named;

@FacesConfig
@Named("titleController")
@RequestScoped
public class TitleController {
	
	private String pageTitle;
	
	public TitleController() {
		pageTitle = "Real Estate Agency";
	}

	public String getPageTitle() {
		return pageTitle;
	}

	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}
	
	@PostConstruct
	public void init() {
		pageTitle = "Real Estate Agency";
	}
}