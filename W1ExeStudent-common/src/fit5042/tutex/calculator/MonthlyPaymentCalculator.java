package fit5042.tutex.calculator;

import javax.ejb.Remote;

@Remote
public interface MonthlyPaymentCalculator {
	double calculate(double principle, int year, double interestRate);
}
